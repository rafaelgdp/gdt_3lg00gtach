extends KinematicBody2D
class_name scr_states

export (float) var walkSpeed	= 8
export (float) var jumpSpeed	= -8
export (float) var gravity		= 0.5
export (float) var changeAremainder = 0.5
export (float) var changeVinitial = -30

enum states {idle, walking, jumping, attackingPlain, crouching, sliding, falling}
var state = states.idle
var isStateChanged = false
var previousState


"""
tags:
	these are abstractions of states, they are not states, so no isIdle tag.
	unles idle becomes a tag in itself... im still ironing this out
	
	shadow tags are so that tag changes dont take immedeate effect (denoted by stag)
	I refresh shadow tags at the beginning of _process, so that the actions that take place are dependant on what state the character was in at the beginning of the frame.
	Shadow tags are to be read only. as if the tags cast a shadow.
	
	maybe make complex tags that depend on true/false values of various tags and do away with state variable. but for now state simplifies things in a way.
"""

# [< comment date 2020 Apr 01 >] consider getset, and deferred calls

var tagGrounded    : bool = true
var tagAirbourne   : bool = false
var tagMovingH     : bool = false
var tagMovingV     : bool = false
var tagJumping     : bool = false
var tagAttacking   : bool = false
var tagHunkered    : bool = false
var tagMoonJumping : bool = false
var tagMjInitial   : bool = false

var stagGrounded    : bool
var stagAirbourne   : bool
var stagMovingH     : bool
var stagMovingV     : bool
var stagJumping     : bool
var stagAttacking   : bool
var stagHunkered    : bool
var stagMoonJumping : bool
var stagMjInitial   : bool

var stagDicionary : Dictionary
#end tags



var moveVector    : Vector2 = Vector2(0,0)
var gravityVector : Vector2
var moveCollision : KinematicCollision2D
var inputMoveDirection : Vector2 = Vector2() 

#for moonjump
var timepassed    : float
var isJumpbuttonReleasedYet : bool
var timeHoldIn1 : float = 0.3
var timeHoldIn2 : float = 0.6
var case
var timelimit  : float
var timelimitA : float = 0.4
var timelimitB : float = 0.8
var timelimitC : float = 1.2

#For enemy fighting queue
var enemyFighterHead : Enemy
var enemyFighterTail : Enemy

func _init():
	variableKeepPlayerCharacter.thePlayerCharacter = self

func _ready():
	gravityVector = Vector2(0,gravity)

func _process(delta):
	castShadowTags()
	determineInputMoveDirection()
	isStateChanged = false
	moveCollision = null
	
	match state:
		states.idle:
#			print("Character is idle")
			actions_idle()
			
		states.walking:
#			print("Character is walking")
			actions_walking()
			
		states.jumping:
			actions_jumping()
			
		states.falling:
			pass
			
		_:
			print("Character is in an unknown state")
			pass
	
	if stagAirbourne:
		passive_airbourne()
		
	if stagGrounded:
		passive_grounded()
		
	if stagMovingH || stagMovingV:
		passive_move()
		
	if stagMoonJumping:
		passive_moonjump(delta)
		
	if stagMjInitial:
		passive_moonjumpinitial(delta)
	
	determineState()
	
### ACTIONS
func actions_idle():
	if Input.is_action_pressed("ui_MainAction"):
		tagAttacking = true
		
		
func actions_walking():
	if Input.is_action_pressed("ui_MainAction"):
		tagAttacking = true
	#pass
		
func actions_jumping():
	
	if Input.is_action_pressed("ui_MainAction"):
		tagAttacking = true
		
#Improve this
func actions_attackingPlain():
	var atek: Node2D = preload("res://characters/AttackArea.tscn").instance()
	atek.position = position + Vector2(32, 0)
	get_tree().current_scene.add_child(atek)
	
	
func action_crouching():
	if Input.is_action_pressed("ui_MainAction"):
		tagAttacking = true
	
func action_sliding():
	if Input.is_action_pressed("ui_MainAction"):
		tagAttacking = true


###Passive Functions
func passive_airbourne():
	#gravity takes effect
	if !stagMjInitial:
		moveVector.y += gravity
	
	
	#air move horizontal
	if inputMoveDirection.x != 0:
		moveVector.x = inputMoveDirection.x * walkSpeed
		tagMovingH = true
	else:
		moveVector.x = 0
		tagMovingH = false
	
	# I check for ground in the passive move funtion since it relies on the move collision. this tag stuff is confusing.
		#perhaps I should make pre move and post move functions, but that seems a bit drastic for now
		#perhaps I should create a complex tag that combines move and airbourne, move and grounded; but that seems a bit drastic for now
	
func passive_grounded():
	
	#check if airbourne
	if test_move(transform, Vector2(0,1)) == false:
		tagGrounded = false
		tagAirbourne = true
		tagMovingV = true
		print("begin falling")
		
	#walk
	if inputMoveDirection.x != 0:
		moveVector.x = inputMoveDirection.x * walkSpeed
		tagMovingH = true
	else:
		moveVector.x = 0
		tagMovingH = false
	
	#jump
	if inputMoveDirection.y == -1:
		#Begin jump
		#moveVector.y = jumpSpeed
		tagAirbourne = true
		tagJumping   = true
		tagGrounded  = false
		tagMovingV   = true
		
		#Enter moonjump
		tagMoonJumping = true
		tagMjInitial   = true
		# changeV, changeA, timepoint{1,2,3}, timelimit{A,B,C},  should be set
		timepassed = 0
		isJumpbuttonReleasedYet = false
		timelimit = timelimitA
		case = "A"
		print("enter moonjump!")
		
		
		
	if inputMoveDirection.y == 1:
		tagHunkered = true

func passive_move():
	#print(moveVector.y)
	moveCollision = move_and_collide(moveVector)
	if moveCollision != null:
		
		#repeatedly move along surfaces
		var theNormal : Vector2 = moveCollision.get_normal()
		while moveCollision.remainder.slide(theNormal) != Vector2(0,0):
			var newMove = moveCollision.remainder.slide(theNormal)
			moveCollision = move_and_collide(newMove)
			if moveCollision == null:
				break
			theNormal = moveCollision.get_normal()
		"""
		maybe turn these collision based code into a function and use it recursively, because I should still check for walls
		and stuff within the above while loop.
		
		"""
		
		
		
		#check wall
		if stagGrounded:
			if !checkSlope(theNormal):
				moveVector.x = 0
				tagMovingH = false
				print("wall!")
				pass
		
		#check ground
		#if falling down
		#Transplanted from passive Airbourne because I want there to be one move function only, but also now I have a random airbourne check that's out of place. but the check relies on the movement collision
		if moveVector.y > 0 && stagAirbourne && checkSlope(theNormal):
			tagGrounded  = true
			tagJumping   = false
			tagAirbourne = false
			tagMoonJumping = false
			tagMjInitial = false
			tagMovingV   = false
			moveVector.y = 0
			gravity = 0.5
			print( "Peach plum pear")
			
		#move on slopes
#		#if moveVector.x != 0 && stagGrounded && checkSlope(theNormal) && theNormal != Vector2(0,-1):
#
#			#var newMove #= Vector2(-theNormal.y, theNormal.x)
#			var newMove = moveCollision.remainder.slide(theNormal)
#			#move_and_collide(newMove * moveCollision.remainder)
#			move_and_collide(newMove)
#			#print("normal:", theNormal)
#			#print("newMove:",newMove)

func passive_moonjump(delta):
	timepassed += delta
	#print ("chillin moonjump")
	pass
	
func passive_moonjumpinitial(delta):
	print("moonjump time: ",timepassed)
	if Input.is_action_just_released("ui_up"):
		isJumpbuttonReleasedYet = true
	
	#determine case
	if !isJumpbuttonReleasedYet:
		if timepassed >= timeHoldIn2:
			case = "C"
			timelimit = timelimitC
		elif timepassed >= timeHoldIn1:
			case = "B"
			timelimit = timelimitB
			
	#end initial phase or business as usual
	print("changeVinitial: ", changeVinitial)
	if timepassed >= timelimit:
		tagMjInitial=false
		moveVector.y += changeVinitial * (timelimit + delta - timepassed)
		gravity += changeAremainder * (timelimit + delta - timepassed)
	else:
		moveVector.y += changeVinitial * delta
		gravity += changeAremainder * delta 

func castShadowTags():
	#I forsee optimisations may be done here
	stagGrounded  = tagGrounded
	stagAirbourne = tagAirbourne
	stagMovingH   = tagMovingH
	stagMovingV   = tagMovingV
	stagJumping   = tagJumping
	stagAttacking = tagAttacking
	stagHunkered  = tagHunkered
	stagMoonJumping = tagMoonJumping
	stagMjInitial = tagMjInitial
	
	stagDicionary = {
		grounded  = tagGrounded,
		airbourne = tagAirbourne,
		movingH   = tagMovingH,
		movingV   = tagMovingV,
		jumping   = tagJumping,
		attacking = tagAttacking,
		hunkered  = tagHunkered,
		moonJumping = tagMoonJumping,
		mjInitial = tagMjInitial
	}
	
func determineInputMoveDirection():
	inputMoveDirection = Vector2(0,0)
	
	if Input.is_action_pressed("ui_right"):
		inputMoveDirection.x += 1
	if Input.is_action_pressed("ui_left"):
		inputMoveDirection.x -= 1
	if Input.is_action_pressed("ui_up"):
		inputMoveDirection.y -= 1
	if Input.is_action_pressed("ui_down"):
		inputMoveDirection.y += 1

func determineState():
	
	match stagDicionary:
		{"grounded": true, "airbourne": false, "movingH": false, "movingV": false, "jumping": false, "attacking": false, ..}:
			state = states.idle
			if previousState != state:
				previousState = state
				isStateChanged = true
				print("character state determined as idle!")
			
		{"grounded": true, "airbourne": false, "movingH": true, "movingV": false, "jumping": false, "attacking": false, ..}:
			state = states.walking
			if previousState != state:
				previousState = state
				isStateChanged = true
				print("character state determined as walking")
			
		{"grounded": false, "airbourne": true, "movingV": true, "jumping": true, ..}:
			state = states.jumping
			if previousState != state:
				previousState = state
				isStateChanged = true
				print("character state determinated as jumping")
				
		{"grounded": false, "airbourne": true, "jumping": false, ..}:
			state = states.falling
			if previousState != state:
				previousState = state
				isStateChanged = true
				print("character state determined as falling!")
				
				
		_:
			if isStateChanged == false:
				previousState = state
				isStateChanged = true
				print("character state determination failed :(")
				print("dup")
				print("here's the stat:", stagDicionary)
			pass

func checkSlope(theNormal):
	#checks if normal of collision is facing upwards (checking if its a floor or slope)
	var vector_1 = Vector2( 0.9,-1) #0.9 is for a little bit of leniency
	var vector_2 = Vector2(-0.9,-1)
	
	var report = (vector_1.dot(theNormal) >= 0) && (vector_2.dot(theNormal) >= 0)
	return report
	
func receiveKnockback(v):
	print("EINA")
	move_and_collide(Vector2(-50,0))
	
func GetHit(hitValue, knockbackVector):
		print_debug("EINA! I, Player Character " + name + " am gehit with a value of: " + str(hitValue))
		move_and_collide(knockbackVector)

