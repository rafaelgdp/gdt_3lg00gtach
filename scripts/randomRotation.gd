extends Sprite

var timeGauge = 0
var initialPosition
var initialRotation
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	initialPosition = get_position()
	initialRotation = get_rotation()

	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	timeGauge += delta
	if (timeGauge >= 0.1):
		timeGauge -= 0.1
		position = initialPosition + Vector2(rand_range(0,1), rand_range(0,1))*rand_range(10,20)
		rotation = initialRotation + rand_range(-0.2,0.2)


#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
