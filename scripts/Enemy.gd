extends KinematicBody2D
class_name Enemy

# [< comment date 2020 Apr 01 >] GDScript currently doesn't allow cyclic referencing so enemyFighting may not be set with static typing
#	see https://github.com/godotengine/godot/issues/21461
#onready var enemyFighting : EnemyFighting = $EnemyFighting # might be null and thats ok

onready var enemyFighting = $EnemyFighting # might be null and thats ok, not statically typed because GDScript

func _ready():
	pass
	
func Attack():
	pass
	
	
"""
Simple solution for now, move and collide, assume success.

later target position and current position
	potentially with signal(s)
	GoToPosition should be Timer / Tween object like, initial code goes on without it
	
even later, Condition for failing to reach position
"""
func GoToPositon(thePosition : Vector2):
	var delta = thePosition - position
	move_and_collide(delta)



