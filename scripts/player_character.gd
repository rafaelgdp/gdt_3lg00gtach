class_name PlayerCharacter
extends KinematicBody2D
"""
Creation date: 2020 Apr 09
Author: EthanLR (Megaguy32)
Description: code to make the main character object work

this code tries to follow PEP 8 coding style, recommended by Godot documentation (3.2)
"""

"""
tags:
	these are abstractions of states, they are not states, so no isIdle tag.
	unles idle becomes a tag in itself... im still ironing this out
	
	shadow tags are so that tag changes dont take immedeate effect (denoted by stag)
	I refresh shadow tags at the beginning of _process, so that the actions that take place are dependant on what state the character was in at the beginning of the frame.
	Shadow tags are to be read only. as if the tags cast a shadow.
	
	maybe make complex tags that depend on true/false values of various tags and do away with state variable. but for now state simplifies things in a way.
		
		
[< Comment date 2020 Apr 09 >]
	I'm removing shadow tags in favour of getters, setters & deferred calls
	because deferred calls on the idle frame, which is the time before 
		_process is called for all nodes.
		
		classes/class_scenetree.html?highlight=idle frame
		getting_started/scripting/gdscript/gdscript_basics.html#setters-getters
			
	As for tags, I'm thinking of enumerating them and placing them in an
	array, then just check if a tag is contained in the array.
	...
	did some research and nope, dictionary is functionally better
	in this case. as I'm able to make a check if dict contains variable
	tags in one fell swoop.
	
	and im going for the complex tags idea, where tags rely on
	other tags being true, getting rid of absolute states.
"""

# Section 05. signals:
# - - - end section - - -


# Section 06. enums
# - - - end section - - -


# Section 07. constants

const ATTACK_NODE: PackedScene = preload("res://characters/AttackArea.tscn")

# - - - end section - - -


# Section 08. exported variables

export (float) var walk_speed: float = 8
export (float) var jump_speed: float = -8
#export (float) var change_a_remainder: float = 0.5
#export (float) var change_v_initial: float = -30
export (float) var dampen := 1
export (Vector2) var gravity := Vector2(0, 0.5)

# - - - end section - - -


# Section 09. public variables

# For enemy fighting queue
		# TODO: Consider making modular, like a "Queue here" extention object
var enemy_fighter_head : Enemy
var enemy_fighter_tail : Enemy

var move_velocity: Vector2 = Vector2(0,0)
var move_collision: KinematicCollision2D = null
var input_vector: Vector2 = Vector2()

var tags: Tags = Tags.new({
	# simple tags
	"airbourne": false,
	"moving": false,
	"attacking": false,
	"hunkered": false,
	"moon_jumping": false,
	"moon_jumping_initial": false,
	"hurting": false,
	
	# complex tags
	"idle": false,
	"walking": false,
	"jumping": false,
	"attackingPlain": false,
	"crouching": false,
	"sliding": false,
	"falling": false,
})

# - - - end section - - -


# Section 10. private variables
# A.K.A Please don't touch these locally
# - - - end section - - -


# Section 11. onready variables
# - - - end section - - -


# Section 12. optional built-in virtual _init method
func _init():
	variableKeepPlayerCharacter.player_character = self

# - - - end section - - -


# Section 13. built-in virtual _ready method

func _ready():
	pass

# - - - end section - - -


# Section 14. remaining built-in virtual methods

func _process(delta):
#	isStateChanged = false
	determine_input_vector()
	
	# [< Comment date 2020 Apr 10 >] TODO: environment check
	var is_collided := test_move(transform, Vector2(0, 1))
	tags.get_to_write()["airbourne"] = !is_collided
	
	move_collision = null
	
	# [< Comment date 2020 Apr 10 >] TODO: look into unhandled input
	if Input.is_action_pressed("ui_accept"):
		tags.get_to_write()["attacking"] = true
	
	# apply stuff based on tag info
	# apply auxillery data to visible properties?
	
	var is_tag_handled := false #use if wildcard match runs despite being handled
	var tag_hash = tags.get_to_write().hash()
	var debug_flag_should_report_passive_handle := true 
	var report_passive_handle := ""
	
	match tags.get_to_read():
		# complex tags excluded
		{"airbourne": true, ..}:
			is_tag_handled = true
			passive_airbourne()
			continue
			
		{"airbourne": false, ..}:
			is_tag_handled = true
			passive_grounded()
			continue
			
		{"moving": true, ..}:
			is_tag_handled = true
			passive_move()
			continue
			
		{"moon_jumping": true, ..}:
			is_tag_handled = true
			passive_moonjump(delta)
			continue
			
		{"moon_jumping_initial": true, ..}:
			is_tag_handled = true
			passive_moonjumpinitial(delta)
			continue
			
		{"attacking": true, ..}:
			is_tag_handled = true
			passive_attack()
			continue
			
		# complex tags included
		{"idle": true, ..}:
			is_tag_handled = true
			passive_idle()
			continue
			
		{"walking": true, ..}:
			is_tag_handled = true
			passive_walking()
			continue
			
		{"jumping": true, ..}:
			is_tag_handled = true
			passive_jumping()
			continue
			
		{"falling": true, ..}:
			is_tag_handled = true
			continue
			
		_:
			if is_tag_handled == false:
				print_debug("I, a PlayerCharacter named " + name + ", am in an unhandled state (tag combo)")
				print_debug("tags: " + str(tags.get_to_read()))
				
	if debug_flag_should_report_passive_handle:
		if tag_hash != tags.get_to_write().hash(): #tags changed
			if is_tag_handled == false:
				report_passive_handle = "I, a PlayerCharacter named " + name + ", am in an unhandled state (tag combo)"
			print_debug(report_passive_handle)
	
	call_deferred("determine_complex_tags")
	
	


# - - - end section - - -


# Section 15. public methods

# [< Comment date 2020 Apr 10 >] TODO: rethink actions and passive. Think about cause of tags, and impact of tags. and goal based programming
# Passive Functions	

func passive_airbourne():
	# gravity takes effect
	move_velocity += gravity

	# [< Comment date 2020 Apr 10 >] TODO: consider implications with simplified physics movement system
	# air move horizontal
#	if input_vector.x == 0:
#		move_velocity.x = 0
#		tags.get_to_write()["moving"] = false
#	else:
#		move_velocity.x = input_vector.x * walk_speed # [< Comment date 2020 Apr 10 >] TODO: consider move per second
#		tags.get_to_write()["moving"] = true
		
# [< Comment date 2020 Apr 10 >] TODO: improve all this
func passive_grounded():
	
	move_velocity.y = 0
	# check if airbourne
	# [< Comment date 2020 Apr 10 >] TODO: improve this
	if test_move(transform, Vector2(0,1)) == false:
		tags.get_to_write()["airbourne"] = true
		tags.get_to_write()["moving"] = true
		print_debug("I, a PlayerCharacter named " + name + ", have begun falling")
		
	# walk
	if input_vector.x != 0:
		move_velocity.x = input_vector.x * walk_speed # [< Comment date 2020 Apr 10 >] TODO: consider move per second
		tags.get_to_write()["moving"] = true
	else:
		move_velocity.x = 0
		tags.get_to_write()["moving"] = false
	
	# jump
	if input_vector.y == -1:
		#Begin jump
		#move_velocity.y = jumpSpeed
		tags.get_to_write()["airbourne"] = true
		tags.get_to_write()["jumping"] = true
		tags.get_to_write()["moving"] = true
		
		#Enter moonjump
		tags.get_to_write()["moon_jumping"] = true
		tags.get_to_write()["moon_jumping_initial"] = true
		# [< Comment date 2020 Apr 10 >] TODO: fix this
		# changeV, changeA, timepoint{1,2,3}, timelimit{A,B,C},  should be set
#		timepassed = 0
#		isJumpbuttonReleasedYet = false
#		timelimit = timelimitA
#		case = "A"
#		print("enter moonjump!")
		
	if input_vector.y == 1:
		tags.get_to_write()["hunkered"] = true


func passive_move():
	#print(move_velocity.y)
	move_collision = move_and_collide(move_velocity)
	move_velocity.x = move_toward(move_velocity.x, 0, dampen)
#	if move_collision != null:
#
#		#repeatedly move along surfaces
#		var the_normal : Vector2 = move_collision.get_normal()
#		while move_collision.remainder.slide(the_normal) != Vector2(0,0):
#			var new_move = move_collision.remainder.slide(the_normal)
#			move_collision = move_and_collide(new_move)
#			if move_collision == null:
#				break
#			the_normal = move_collision.get_normal()
#		"""
#		maybe turn these collision based code into a function
#		and use it recursively, because I should still check for walls
#		and stuff within the above while loop.
#		"""

#		#check wall
#		if tags.get_to_read()["grounded"]:
#			if !checkSlope(the_normal):
#				move_velocity.x = 0
#				tags.get_to_write()["moving"] = false
#				print("wall!")
#				pass
#
#		#check ground
#		#if falling down
#		#Transplanted from passive Airbourne because I want there to be one move function only, but also now I have a random airbourne check that's out of place. but the check relies on the movement collision
#		if move_velocity.y > 0 and tags.get_to_read()["airbourne"] and checkSlope(the_normal):
#			tags.get_to_write()["grounded"] = true
#			tags.get_to_write()["jumping"] = false
#			tags.get_to_write()["airbourne"] = false
#			tags.get_to_write()["moon_jumping"] = false
#			tags.get_to_write()["moon_jumping_initial"] = false
#			tags.get_to_write()["moving"] = false
#
#			move_velocity.y = 0
#			gravity = Vector2(0,0.5)
#			print( "Peach plum pear")
#
#		#move on slopes
##		#if move_velocity.x != 0 && stagGrounded && checkSlope(the_normal) && the_normal != Vector2(0,-1):
##
##			#var new_move #= Vector2(-the_normal.y, the_normal.x)
##			var new_move = move_collision.remainder.slide(the_normal)
##			#move_and_collide(new_move * move_collision.remainder)
##			move_and_collide(new_move)
##			#print("normal:", the_normal)
##			#print("new_move:",new_move)


func passive_moonjump(delta): pass
#	timepassed += delta
	#print ("chillin moonjump")


func passive_moonjumpinitial(delta): pass
#	print("moonjump time: ",timepassed)
#	if Input.is_passive_just_released("ui_up"):
#		isJumpbuttonReleasedYet = true
#
#	#determine case
#	if !isJumpbuttonReleasedYet:
#		if timepassed >= timeHoldIn2:
#			case = "C"
#			timelimit = timelimitC
#		elif timepassed >= timeHoldIn1:
#			case = "B"
#			timelimit = timelimitB
#
#	#end initial phase or business as usual
#	print("changeVinitial: ", changeVinitial)
#	if timepassed >= timelimit:
#		tagMjInitial=false
#		move_velocity.y += changeVinitial * (timelimit + delta - timepassed)
#		gravity += changeAremainder * (timelimit + delta - timepassed)
#	else:
#		move_velocity.y += changeVinitial * delta
#		gravity += changeAremainder * delta 


# [< Comment date 2020 Apr 10 >] TODO: Improve this, consider do on attacking initial signal
func passive_attack():
	var attack_node: Node2D = ATTACK_NODE.instance()
	attack_node.position = position + Vector2(32, -24)
	get_tree().current_scene.add_child(attack_node)
	tags.get_to_write()["attacking"] = false


func passive_idle(): pass


func passive_walking(): pass


func passive_jumping(): pass


func passive_crouching(): pass


func passive_sliding(): pass

# [< Comment date 2020 Apr 10 >] TODO: consider implemementing input using input map
# Get input from keyboard and put it in vector form
func determine_input_vector():
	input_vector = Vector2(0,0)
	
	if Input.is_action_pressed("ui_right"):
		input_vector.x += 1
	if Input.is_action_pressed("ui_left"):
		input_vector.x -= 1
	if Input.is_action_pressed("ui_up"):
		input_vector.y -= 1
	if Input.is_action_pressed("ui_down"):
		input_vector.y += 1
		
	input_vector = input_vector.normalized()
	
	if input_vector != Vector2(0, 0):
		move_velocity.x = input_vector.x * walk_speed
		
	var is_moving := move_velocity != Vector2(0,0)
	tags.get_to_write()["moving"] = is_moving

"""
[< Comment date 2020 Apr 10 >] TODO:
		There might be a problem here. since I'm using deferred calls
		the tags only get updated at the end,
		but the entire dictionary changes in the deferred function,
		so some tags may be overwritten.
		Consider testing.
		...
		yup, I was right. it is a problem. eish.
		Might actually be a bigger problem that affects others too.
		
		because if you setget a dictionary
		e.g.
			dict = {0: false} setget setter
			
			setter():
				pass
				
		and do elsewhere: myobj.dict[0] = true 
		
		then dict[0] is true anyway even though the setter should prevent that.
		TODO:
			tell the godot community about that
		...
		Github issue, https://github.com/godotengine/godot/issues/37737
		
		Apparently its not a bug? that confuses me. but ok its workaround time.
		and the simplest solution is to bring the shadow tag back, but
		now its a duplicate dicionary, and its written to not read from,
		still making use of a deferred call.
		...
		
		Ok ok ok, it took a few brain farts but I got the desired effect with 
		setget. by returning a duplicate  
		...
		too much fart. check in later.
"""
func determine_complex_tags():
	
	# [< Comment date 2020 Apr 10 >] TODO: consider emiting state (tag combo / tags hash) changed signal
	# [< Comment date 2020 Apr 10 >] TODO: consider enumerating tags for code completion sake
#	var th = tags.get_to_write().hash()
	# instead of using tag get to read, use get to write for reading and writing, because complex tags depend on simple tags, which may be outdated, this method should be called just before Tags applies its unapplied tags
	var is_tag_handled := false
	var tag_hash = tags.get_to_write().hash()

	var debug_flag_should_report_complex_tags := true 
	var complex_tag_report := ""
	match tags.get_to_read():
		{"airbourne": false, "moving": false, "jumping": false, "attacking": false, ..}:
			is_tag_handled = true
			tags.get_to_write()["idle"] = true
			complex_tag_report += ("I, a PlayerCharacter named " + name + ", am idle\n")
			continue
			
		{"airbourne": false, "moving": true, "jumping": false, "attacking": false, ..}:
			is_tag_handled = true
			tags.get_to_write()["walking"] = true
			complex_tag_report += ("I, a PlayerCharacter named " + name + ", am walking\n")
			continue
			
		{"airbourne": true, "movingV": true, "jumping": true, ..}:
			is_tag_handled = true
			tags.get_to_write()["jumping"] = true
			complex_tag_report += ("I, a PlayerCharacter named " + name + ", am jumping\n")
			continue
				
		{"airbourne": true, "jumping": false, ..}:
			is_tag_handled = true
			tags.get_to_write()["falling"] = true
			complex_tag_report += ("I, a PlayerCharacter named " + name + ", am falling\n")
			continue
				
				
#		_:
#			if is_tag_handled == false: pass
#				print_debug("I, a PlayerCharacter named " + name + ", have not determined any complex tags")
#				print_debug("here's the tags:", str(tags.get_to_read()))
	
	if debug_flag_should_report_complex_tags:
		if tag_hash != tags.get_to_write().hash(): #tags changed
			if is_tag_handled == false:
				complex_tag_report = "I, a PlayerCharacter named " + name + ", have not determined any complex tags"
			print_debug(complex_tag_report)
			
			
func check_if_slope(the_normal):
	#checks if normal of collision is facing upwards (checking if its a floor or slope)
	var vector_1 = Vector2( 0.9,-1) #0.9 is for a little bit of leniency
	var vector_2 = Vector2(-0.9,-1)
	
	var report = (vector_1.dot(the_normal) >= 0) and (vector_2.dot(the_normal) >= 0)
	return report


func receiveKnockback(v):
	print("EINA")
	move_and_collide(Vector2(-50,0))


func hurt(hit_value: float, knockback: Vector2):
		print_debug("I, Player Character " + name + " am gehit with a value of: " + str(hit_value))
		tags.get_to_write()["hurting"] = true

# - - - end section - - -


# Section 16. private methods
# A.K.A Please don't touch these locally
# - - - end section - - -


# [< Comment date 2020 Apr 10 >] TODO: consider making this its own node in editor
class Tags:
	extends Object
	
	var _tags: Dictionary
	var _tags_unapplied: Dictionary
	
	func _init(dict: Dictionary) -> void:
		_tags = dict.duplicate()
		_tags_unapplied = dict.duplicate()
	
	func get_to_read() -> Dictionary:
		return _tags.duplicate()
	
	func get_to_write() -> Dictionary:
		call_deferred("_apply")
		return _tags_unapplied
		
	func _apply() -> void:
		_tags = _tags_unapplied.duplicate()


func _on_Button_pressed():
	print("ge press")
	tags.get_to_write()["attacking"] = true
