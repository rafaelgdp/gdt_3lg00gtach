tool
extends EditorScript

var dict: Dictionary = {0: false, 1: false, 2: false} setget setter

func _run():
	print("0.")
	print("initial: " + str(dict))
	
	print("\n1.")
	dict[2] = true
	#setter({0:true, 1:false})
	self.dict[1] = true
	
	print("\n2.")
	print_debug("final: " + str(dict))
	
func setter(val: Dictionary) -> void:
	print("deferring")
	print("so far: " + str(dict))
	call_deferred("_setter", val)
	
	
func _setter(val: Dictionary):
	print("deferred")
	dict = val

