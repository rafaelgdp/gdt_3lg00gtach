tool
extends EditorScript

var test_dict: Dictionary = {0: true, 1: true} setget dict_set

func _run():
	self.test_dict[1] = false
	
func dict_set(val) -> void:
	print_debug(val)
	test_dict = val
